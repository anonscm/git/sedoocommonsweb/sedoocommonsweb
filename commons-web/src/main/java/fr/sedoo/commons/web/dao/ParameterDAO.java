package fr.sedoo.commons.web.dao;

public interface ParameterDAO {
	
	static final String PARAMETER_DAO_BEAN_NAME = "parameterDAO";
	
	String getParameter(String parameterName);

}
