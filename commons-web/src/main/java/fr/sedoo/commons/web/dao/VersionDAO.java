package fr.sedoo.commons.web.dao;

public interface VersionDAO {
	
	static final String VERSION_DAO_BEAN_NAME = "versionDAO";
	static final String VERSION_PARAMETER_NAME = "version";
	String getVersion();

}
