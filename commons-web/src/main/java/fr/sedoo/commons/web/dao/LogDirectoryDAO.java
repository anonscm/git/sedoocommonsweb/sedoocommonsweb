package fr.sedoo.commons.web.dao;

public interface LogDirectoryDAO {
	
	static final String LOG_DIRECTORY_DAO_BEAN_NAME = "logDirectoryDAO";
	static final String LOG_DIRECTORY_PARAMETER_NAME = "logDirectory";
	String getLogDirectory();

}
