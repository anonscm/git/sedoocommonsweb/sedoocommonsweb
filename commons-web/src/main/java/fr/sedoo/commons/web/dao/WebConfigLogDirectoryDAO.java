package fr.sedoo.commons.web.dao;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import fr.sedoo.commons.web.servlet.ServletContextProvider;

public class WebConfigLogDirectoryDAO implements VersionDAO{

	@Override
	public String getVersion() {
		ServletContext context = ServletContextProvider.getContext();
		String initParameter = context.getInitParameter("logDirectory");
		if (StringUtils.isEmpty(initParameter))
		{
			return "";
		}
		else
		{
			return initParameter.trim();
		}
	}
	
	
	
	

}
