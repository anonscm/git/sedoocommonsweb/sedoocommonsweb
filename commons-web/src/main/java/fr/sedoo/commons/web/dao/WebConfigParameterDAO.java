package fr.sedoo.commons.web.dao;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import fr.sedoo.commons.web.servlet.ServletContextProvider;

public class WebConfigParameterDAO implements ParameterDAO{
	
	
    @Override
	public String getParameter(String parameterName) {
		ServletContext context = ServletContextProvider.getContext();
		if (context == null)
		{
			return "";
		}
		String initParameter = context.getInitParameter(parameterName);
		if (StringUtils.isEmpty(initParameter))
		{
			return "";
		}
		else
		{
			return initParameter.trim();
		}
	}

}
